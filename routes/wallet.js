var express = require('express');
var iPay88Signature = require('../public/js/sha1.js');
var router = express.Router();

/* GET home page. */
router.post('/get_my_wallet', function (req, res, next) {
  /*
    user:用户的id
  */
  var obj = {
    user: req.session.m_id
  }
  var md5Str = toSingy(obj)
  if (md5Str == req.headers.singy) {
    sqlQuery('member_table', 'member_money', {
      member_id: req.session.m_id
    }, function (result) {
      var respond = {
        ret: 200,
        data: {
          money: result[0].member_money //用户的余额
        }
      }
      res.json(respond);
    });
  } else {
    var respond = {
      ret: 201,
      data: {}
    }
    res.json(respond);

  }
});
//我的钱包

router.post('/recharge', function (req, res, next) {
  /*
    user:用户的id,
    account:充值的金额
  */
  var obj = {
    user: req.session.m_id,
    account: req.body.account
  }
  var md5Str = toSingy(obj)
  if (md5Str == req.headers.singy) {
    //插入充值记录
    var token=getToken();//定义Token 
    var ordersn=(getNowTime() + generateMixed(6)).replace('_','');
    sqlIns('money_recharge', {
      type: 1,
      money_sess: ordersn,
      money_id: req.session.m_id,
      money_add: req.body.account,
      money_reason: language.money_recharge[0],
      modify_id: req.session.m_id,
      modify_ip: getClientIp(req),
      register_date: getNowTime(),
      token: token
    }, function (result) {
      var ipay88Singy=iPay88Signature.iPay88Signature(paymentConfig.paymentKey+paymentConfig.paymentCode+ordersn+(req.body.account*100)+'PHP');
      //插入充值申请成功，生成支付链接
      let ResponseURL=encodeURIComponent(paymentConfig.ResponseURL+'?token='+token+'&uid='+result.insertId+'&type=response');
      let BackendURL=encodeURIComponent(paymentConfig.BackendURL+'?token='+token+'&uid='+result.insertId+'&type=back');
      let urlStr='http://www.phmall.com.ph/test/payment/index.php?MerchantCode='+paymentConfig.paymentCode+'&PaymentId='+paymentConfig.paymentId+'&RefNo='+ordersn+'&Amount='+toFloat(req.body.account)+'&UserName='+req.session.m_id+'&UserEmail=5503903@qq.com&UserContact=8617750935360&Remark=&Signature='+ipay88Singy+'&ResponseURL='+ResponseURL+'&BackendURL='+BackendURL;
      var respond = {
        ret: 200,
        data: {
          URL:urlStr
        }
      }
      res.json(respond);
    })
  } else {
    var respond = {
      ret: 201,
      data: {}
    }
    res.json(respond);
  }
});
//充值

router.post('/withdraw', function (req, res, next) {
  /*
    user:用户的id,
    account:充值的金额
  */
  var obj = {
    user: req.session.m_id,
    account: req.body.account
  }
  var md5Str = toSingy(obj)
  if (md5Str == req.headers.singy) {
    //插入充值记录
    var token=getToken();//定义Token 
    var ordersn=(getNowTime() + generateMixed(6)).replace('_','');

    sqlQuery('member_table','*',{
      member_id:req.session.m_id
    },function(result){
      if(result.length>0)
      {
        //插入提现申请-》增加用户的冻结金额，减少用户的可用余额
        let task1 = sqlInsStr('money_widthdraw', {
          type: 1,
          money_sess: ordersn,
          money_id: req.session.m_id,
          money_add: req.body.account,
          money_reason: language.money_withdraw[0],
          modify_id: req.session.m_id,
          modify_ip: getClientIp(req),
          register_date: getNowTime(),
          token: token
        });
        let task2 = sqlUpdateStr('member_table',{
          member_money_freeze:parseFloat(result[0].member_money_freeze)+parseFloat(req.body.account),
          member_money:parseFloat(result[0].member_money- parseFloat(req.body.account))
        },' where member_id="'+req.session.m_id+'"');

        Transaction([task1,task2],function(result){
          if(result.length>0){
            //申请提现成功
            var respond = {
              ret: 200,
              data: {
                status:1
              }
            }
            res.json(respond);
           
          }
          else{
             //申请提现失败
             var respond = {
              ret: 200,
              data: {
                status:0
              }
            }
            res.json(respond);
          }
        }) 

      }
      
    })
    
    
  } else {
    var respond = {
      ret: 201,
      data: {}
    }
    res.json(respond);
  }
});
//提现

router.post('/get_log_all', function (req, res, next) {
  /*
    user:用户的id
  */
  var obj = {
    user: req.session.m_id
  }
  var md5Str = toSingy(obj)
  if (md5Str == req.headers.singy) {
    sqlQuery('member_money', '*', {
      money_id: req.session.m_id
    }, function (result) {
      var respond = {
        ret: 200,
        data: {
          log:result
        }
      }
      res.json(respond);
    },'order by uid desc');
  } else {
    var respond = {
      ret: 201,
      data: {}
    }
    res.json(respond);
  }
});//获取记录

router.get('/', function (req, res, next) {
  console.log(iPay88Signature.iPay88Signature("e7O7GAl4vhPH004210011223344500PHP"));
  res.render('wallet', {
    title: 'Express'
  });
});//

module.exports = router;


